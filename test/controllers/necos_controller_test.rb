require 'test_helper'

class NecosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get necos_index_url
    assert_response :success
  end

  test "should get show" do
    get necos_show_url
    assert_response :success
  end

  test "should get new" do
    get necos_new_url
    assert_response :success
  end

  test "should get edit" do
    get necos_edit_url
    assert_response :success
  end

end
