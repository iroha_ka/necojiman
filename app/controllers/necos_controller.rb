class NecosController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  def index
    @necos = Neco.all
  end

  def show
    @neco = Neco.find(params[:id])
  end

  def new
    @neco = Neco.new
  end
  
  def create
    @neco = Neco.new(neco_params)
    @neco.user_id = current_user.id
    if @neco.save
      redirect_to neco_path(@neco), notice: '投稿に成功しました。'
    else
      render :new
    end
  end
  
  def edit
    @neco = Neco.find(params[:id])
    if @neco.user != current_user
      redirect_to necos_path, alert: '不正なアクセスです。'
    end
  end
  
  def update
    @neco = Neco.find(params[:id])
    if @neco.update(neco_params)
      redirect_to neco_path(@neco), notice: '更新に成功しました。'
    else
      render :edit
    end
  end
  
 def destroy
   neco = Neco.find(params[:id])
   neco.destroy
   redirect_to necos_path
 end
  
  private
  def neco_params
    params.require(:neco).permit(:title, :body, :image)
  end
  
end
